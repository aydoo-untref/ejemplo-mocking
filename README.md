Robot
======

Este es un simple ejemplo para mostrar la técnica de mocking.

Se plantea un clase robot que depende de un sensor de hardware y que cuyo fabricante provee una clase para interactuar con el.

El ejemplo utiliza la librería Mockito.


Por dudas y sugerencias contactase a nicopaez@computer.org