package robots;

public class Robot {

	private Direccion direccion;
	private Sensor sensor;

	public Robot(Sensor sensor) {
		this.sensor = sensor;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public void avanzar() {
		if (this.sensor.enColision()) {
			this.direccion = this.direccion.getInversa();
		}
			
	}

	public Direccion getDireccion() {
		return this.direccion;
	}

}
