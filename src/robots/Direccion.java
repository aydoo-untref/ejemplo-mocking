package robots;

public class Direccion {

	private static Direccion norte;
	private static Direccion sur;
	private static Direccion este;
	private static Direccion oeste;
	
	static {
		norte = new Direccion();
		sur = new Direccion();
		este = new Direccion();
		oeste = new Direccion();
		
		norte.setInversa(sur);
		sur.setInversa(norte);
		este.setInversa(oeste);
		oeste.setInversa(este);
	}

	private Direccion inversa;
	
	private Direccion() {}
	
	private void setInversa(Direccion inversa) {
		this.inversa = inversa;
	}

	public static Direccion norte() {
		return norte;
	}

	public Direccion getInversa() {
		return this.inversa;
	}

}
