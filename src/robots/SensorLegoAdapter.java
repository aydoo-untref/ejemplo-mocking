package robots;

public class SensorLegoAdapter implements Sensor {

	@Override
	public boolean enColision() {
		Lego lego = new Lego();
		return lego.estaActivo();
	}

}
