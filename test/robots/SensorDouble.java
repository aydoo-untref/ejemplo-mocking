package robots;

public class SensorDouble implements Sensor {

	
	private boolean resultado;

	public SensorDouble(boolean resultadoSensor) {
		this.resultado = resultadoSensor;
	}

	@Override
	public boolean enColision() {
		return this.resultado;

	}

}
