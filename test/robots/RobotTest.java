package robots;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class RobotTest {

	@Test
	public void avanzarDeberiaCambiarDireccionCuandoSensorDetectaColision() {
		
		Sensor sensor = Mockito.mock(Sensor.class);
		Mockito.when(sensor.enColision()).thenReturn(true);
		
		Direccion direccionNorte = Direccion.norte();
		Robot robot =  new Robot(sensor);
		robot.setDireccion(direccionNorte);
		
		robot.avanzar();
		
		Assert.assertNotEquals(direccionNorte, robot.getDireccion());

	}

	@Test
	public void avanzarNoDeberiaCambiarDireccionCuandoSensorNoDetectaColision() {
		
		Sensor sensor = Mockito.mock(Sensor.class);
		Mockito.when(sensor.enColision()).thenReturn(false);
		
		Direccion direccionNorte = Direccion.norte();
		Robot robot =  new Robot(sensor);
		robot.setDireccion(direccionNorte);
		
		robot.avanzar();
		
		Assert.assertEquals(direccionNorte, robot.getDireccion());

	}
	
}
